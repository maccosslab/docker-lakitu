FROM continuumio/miniconda:4.5.12 as base

MAINTAINER Austin Keller

# Add a non-root user to run the app and create home directory to store config files
RUN groupadd -r appuser && useradd -r -m -g appuser appuser

# Create conda and python setup directories and a working directory
RUN mkdir /app /app/wd && chmod -R 777 /app/wd

# Create the conda environment
###

ADD environment.yml /tmp/environment.yml

RUN ["conda", "env", "create", "-f", "/tmp/environment.yml"]

RUN echo "source activate lakitu" > /home/appuser/.bashrc

ENV PATH /opt/conda/envs/lakitu/bin:$PATH

WORKDIR /app/wd

USER appuser

CMD ["lakitu", "-h"]
