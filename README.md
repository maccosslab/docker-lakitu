# Lakitu - Docker

Prerequisites:

* Docker
* AWS credentials config file

We'll assume that your AWS credentials file is in ~/.aws/credentials ($HOME/.aws/credentials)

## Help

```
$ docker run --rm -it atkeller/lakitu:0.12.0 lakitu --help
```

## Configure Lakitu

```
$ docker run --rm -it \
    --user 999:$(id -g) \
    -v $HOME/.lakitu:/home/appuser/.lakitu \
    -v $HOME/.aws:/home/appuser/.aws \
    atkeller/lakitu:0.12.0 lakitu config
```

This gives lakitu access to your .aws and .lakitu home directories. The lakitu configuration will be written in `~/.lakitu`.

## List pipelines

```
$ docker run --rm -it \
    --user 999:$(id -g) \
    -v $HOME/.lakitu:/home/appuser/.lakitu \
    -v $HOME/.aws:/home/appuser/.aws \
    atkeller/lakitu:0.12.0 lakitu list
```

## Deploy cluster

```
$ docker run --rm -it \
    --user 999:$(id -g) \
    -v $HOME/.lakitu:/home/appuser/.lakitu \
    -v $HOME/.aws:/home/appuser/.aws \
    atkeller/lakitu:0.12.0 lakitu cluster deploy
```

## Run pipeline

### List pipeline parameters

```
$ docker run --rm -it \
    --user 999:$(id -g) \
    -v $HOME/.lakitu:/home/appuser/.lakitu \
    -v $HOME/.aws:/home/appuser/.aws \
    atkeller/lakitu:0.12.0 lakitu run lpcrux 0.2.0 --help
```

### Run

Docker can't take in your local paths directly. Instead, we map your current working directory to a mount at /app/wd in the docker container. If all of your files to run are in your current directory, then simply prefix each file path with /app/wd in the run parameters.

```
$ docker run --rm -it \
    --user 999:$(id -g) \
    -v $HOME/.lakitu:/home/appuser/.lakitu \
    -v $HOME/.aws:/home/appuser/.aws \
    -v $(pwd):/app/wd \
    atkeller/lakitu:0.12.0 lakitu run lpmsconvert 0.3.0 --run-id my_unique_run_id --ms-set /app/wd/mydata.mzML --msconvert-config /app/wd/my_msconvert_config.txt
```


